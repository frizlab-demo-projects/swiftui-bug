/*
 * ViewModel.swift
 * SwiftUIBug
 *
 * Created by François Lamboley on 04/08/2020.
 */

import Combine
import Foundation



class ViewModel : ObservableObject {
	
	@Published
	var names = [Model]()
	
	init() {
		reload()
	}
	
	func reload() {
		names = [
			[Model(id: "object1", value: "value1"),  Model(id: "object2", value: "value1"),  Model(id: "object3", value: "value1")],
			[Model(id: "object1", value: "value2"),  Model(id: "object2", value: "value2"),  Model(id: "object3", value: "value2")],
			[Model(id: "object1", value: "value3"),  Model(id: "object2", value: "value3"),  Model(id: "object3", value: "value3")],
			[Model(id: "object1", value: "value4"),  Model(id: "object2", value: "value4"),  Model(id: "object3", value: "value4")],
			[Model(id: "object1", value: "value5"),  Model(id: "object2", value: "value5"),  Model(id: "object3", value: "value5")],
			[Model(id: "object1", value: "value6"),  Model(id: "object2", value: "value6"),  Model(id: "object3", value: "value6")],
			[Model(id: "object1", value: "value7"),  Model(id: "object2", value: "value7"),  Model(id: "object3", value: "value7")],
			[Model(id: "object1", value: "value8"),  Model(id: "object2", value: "value8"),  Model(id: "object3", value: "value8")],
			[Model(id: "object1", value: "value9"),  Model(id: "object2", value: "value9"),  Model(id: "object3", value: "value9")],
			[Model(id: "object1", value: "value10"), Model(id: "object2", value: "value10"), Model(id: "object3", value: "value10")],
			[Model(id: "object1", value: "value11"), Model(id: "object2", value: "value11"), Model(id: "object3", value: "value11")],
			[Model(id: "object1", value: "value12"), Model(id: "object2", value: "value12"), Model(id: "object3", value: "value12")],
			[Model(id: "object1", value: "value13"), Model(id: "object2", value: "value13"), Model(id: "object3", value: "value13")]
		].randomElement()!
	}
	
}
