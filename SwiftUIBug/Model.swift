/*
 * Model.swift
 * SwiftUIBug
 *
 * Created by François Lamboley on 04/08/2020.
 */

import Foundation



struct Model : Identifiable {
	
	var id: String
	var value: String
	
}
