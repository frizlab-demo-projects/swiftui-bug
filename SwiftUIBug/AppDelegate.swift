/*
 * AppDelegate.swift
 * SwiftUIBug
 *
 * Created by François Lamboley on 04/08/2020.
 */

import Cocoa
import SwiftUI



@main
class AppDelegate: NSObject, NSApplicationDelegate {
	
	var window: NSWindow!
	
	func applicationDidFinishLaunching(_ aNotification: Notification) {
		/* Create the SwiftUI view that provides the window contents. */
		let contentView = ContentView(model: ViewModel())
		
		/* Create the window and set the content view. */
		window = NSWindow(
			contentRect: NSRect(x: 0, y: 0, width: 480, height: 300),
			styleMask: [.titled, .closable, .miniaturizable, .resizable, .fullSizeContentView],
			backing: .buffered, defer: false
		)
		window.isReleasedWhenClosed = false
		window.center()
		window.setFrameAutosaveName("Main Window")
		window.contentView = NSHostingView(rootView: contentView)
		window.makeKeyAndOrderFront(nil)
	}
	
	func applicationWillTerminate(_ aNotification: Notification) {
	}
	
	func applicationShouldTerminateAfterLastWindowClosed(_ sender: NSApplication) -> Bool {
		return true
	}
	
}
