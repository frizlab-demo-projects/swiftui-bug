/*
 * Views.swift
 * SwiftUIBug
 *
 * Created by François Lamboley on 04/08/2020.
 */

import SwiftUI



struct ContentView: View {
	
	@ObservedObject
	var model: ViewModel
	
	var body: some View {
		VStack{
			Text("First select an object in the side bar. Then click the button twice (the value must change at least twice). The sidebar correctly refreshes the value, but the details view does not!")
				.multilineTextAlignment(.center)
				.padding()
			Button("Click Me!"){
				model.reload()
			}
			NavigationView{
				List(model.names){ modelItem in
					NavigationLink(destination: DetailsView(model: modelItem)){ Text(modelItem.id + " - " + modelItem.value) }
				}
				.listStyle(SidebarListStyle())
			}.navigationViewStyle(DoubleColumnNavigationViewStyle())
		}
	}
	
}
	
struct DetailsView : View {
	
	var model: Model
	
	var body: some View {
		HStack{
			Spacer()
			VStack{
				Spacer()
				Text("Value: " + model.value)
				Spacer()
			}
			Spacer()
		}
	}
	
}


struct ContentView_Previews: PreviewProvider {
	static var previews: some View {
		ContentView(model: ViewModel())
	}
}
